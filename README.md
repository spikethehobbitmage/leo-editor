Leo http://leoeditor.com 6.6.3 is now available on
[GitHub](https://github.com/leo-editor/leo-editor/releases) and
[pypi](https://pypi.org/project/leo/).

Leo, http://leoeditor.com is an [IDE, outliner and PIM](http://leoeditor.com/preface.html).

**The highlights of Leo 6.6.3**

- Enhance leoserver.py to support leoInteg and leoJS.
- Improve how Leo handles url's.
- Simplify the GlobalConfigManager class.
- The usual assortment of tweaks and bug fixes.

**Links**

- 6.6.3 issues: https://github.com/leo-editor/leo-editor/issues?q=is%3Aissue+milestone%3A6.6.3+
- [Documentation](http://leoeditor.com/leo_toc.html)
- [Tutorials](http://leoeditor.com/tutorial.html)
- [Video tutorials](http://leoeditor.com/screencasts.html)
- [Forum](http://groups.google.com/group/leo-editor)
- [Download](http://sourceforge.net/projects/leo/files/)
- [Leo on GitHub](https://github.com/leo-editor/leo-editor)
- [LeoVue](https://github.com/kaleguy/leovue#leo-vue)
- [What people are saying about Leo](http://leoeditor.com/testimonials.html)
- [A web page that displays .leo files](http://leoeditor.com/load-leo.html)
- [More links](http://leoeditor.com/leoLinks.html)

